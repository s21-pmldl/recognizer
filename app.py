from flask import Flask, request, flash
from werkzeug.utils import secure_filename
import os
from sklearn.pipeline import make_pipeline
from transformers import ToAudioTransformer, AudioToTextTransformer


UPLOAD_FOLDER = "/tmp/audio/"
PIPE = make_pipeline(ToAudioTransformer(), AudioToTextTransformer("./model"))

app = Flask(__name__)
app.secret_key = os.environ['FLASK_SECRET_KEY']
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['PIPE'] = PIPE


@app.route('/', methods=['POST'])
def textify():
    if 'file' not in request.files:
        flash("No file part")
    file_ = request.files['file']
    if file_.filename == '':
        flash('No selected file')
    filename = secure_filename(file_.filename)
    filepath = os.path.join(app.config['UPLOAD_FOLDER'], filename)
    file_.save(filepath)

    text = app.config['PIPE'].transform(filepath)
    return text


if __name__ == "__main__":
    os.makedirs(app.config['UPLOAD_FOLDER'], exist_ok=True)
    isDev = os.environ.get('FLASK_ENV') == "development"
    app.run(debug=isDev, host='0.0.0.0', port=3000)
