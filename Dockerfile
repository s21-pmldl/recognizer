FROM python:3.9.1-slim
WORKDIR /app

RUN apt update \
 && apt install -y ffmpeg \
 && apt clean

COPY model model

COPY requirements.txt ./
RUN pip3 install -r requirements.txt

COPY transformers.py ./
COPY app.py ./

ENV FLASK_ENV production
ENV FLASK_SECRET_KEY = "al4t;bflw32vfs23"
CMD ["python3", "app.py"]
